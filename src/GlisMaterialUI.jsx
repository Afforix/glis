import React from 'react';
import Button from '@material-ui/core/Button';

class GlisMaterialUI extends React.Component {
    render() {
        return (
            <div>
                <h1>Glis Material-UI</h1>
                <Login/>
                <Client/>
            </div>
        );
    }
}

class Login extends React.Component {
    render() {
        return (
            <div>
                <input type="text" ref="address"/>
                <Button onClick={() => console.log("Connect onClick")}>Connect</Button>
            </div>
        );
    }
}

class Client extends React.Component {
    render() {
        return (
            <div>
                <Rooms rooms={["one", "two", "three", "four", "five"]}/>
                <RoomChat/>
            </div>
        );
    }
}

class Rooms extends React.Component {
    render() {
        return (
            <div>
                <h4>Rooms</h4>
                <ul>
                    {this.props.rooms.map((name, index) => {
                        return (<Room key={name} name={name}/>);
                    })}
                </ul>
            </div>
        );
    }
}

function Room(props) {
    return(
        <li>{props.name}</li>
    );
}

class RoomChat extends React.Component {
    render() {
        return (
            <Messages messages={["Hey", "Hi", "Hello"]}/>
        );
    }
}

class Messages extends React.Component {
    render() {
        return(
            <div>
                <ul>
                    {this.props.messages.map((msg) => {
                        return <Message key={msg} message={msg}/>;
                    })}
                </ul>
            </div>
        );
    }
}

function Message(props) {
    return (
        <li>{props.message}</li>
    );
}

export default GlisMaterialUI;
