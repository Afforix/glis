import React from 'react'
import ReactDOM from 'react-dom';
import App from './App.jsx'
import GlisMaterialUI from './GlisMaterialUI.jsx'
import { BrowserRouter, Route, Link, Switch } from 'react-router-dom'

function E404() {
    return <h1>404</h1>;
}

ReactDOM.render(<BrowserRouter basename="/glis">
                <div>
                    <nav>
                    <ul>
                        <li>
                        <Link to="/">Materialize</Link>
                        </li>
                        <li>
                        <Link to="/material-ui">Material-UI</Link>
                        </li>
                    </ul>
                    </nav>
                    <Switch>
                        <Route exact path="/" component={App}/>
                        <Route path="/material-ui" component={GlisMaterialUI}/>
                        <Route component={E404}/>
                    </Switch>
                </div>
                </BrowserRouter>, 
    document.getElementById("root"));
