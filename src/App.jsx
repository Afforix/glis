import React from 'react'

class App extends React.Component {
    render() {
        return (
            <div>
                <h1>Glis Chat</h1>
                <Login/>
                <LoggingIn/>
                <div className="row">
                    <Rooms/>
                    <Messages/>
                    <ActualRoomInfo/>
                </div>
            </div>
        );
    }
}

class Login extends React.Component {
    render() {
        return (
            <div className='row'>
                <div className="input-field col s4">
                    <input name='username' id='username' placeholder="Your username" type="text" />
                    <label htmlFor="username">Your username</label>
                </div>
                <div className="input-field col s6">
                    <input name='address' id='address' placeholder="Adress of server" type="text" defaultValue='glis.buchticka.eu:1619' />
                    <label htmlFor="address">Adress of server</label>
                </div>
                <div className="input-field col s2">
                    <button className="btn waves-effect waves-light blue" type="submit">
                        Connect
                    </button>
                </div>
            </div>
        );
    }
}

class LoggingIn extends React.Component {
    render() {
        return (
            <div className='row'>
                <div className="progress blue lighten-4">
                    <div className="indeterminate blue"></div>
                </div>
            </div>
        )
    }
}

class Rooms extends React.Component {
    constructor() {
        super();
        this.state = {
            rooms: ["U Nudle", "U Lucky", "School"],
        };
    }

    render() {
        return (
            <div className="col l3 m3 s6" id="rooms">
                <h4>Rooms</h4>
                <div className="collection">
                    {this.state.rooms.map((room, index) => {
                        return (
                            <a href='#!' className="collection-item" key={room}>
                                <span className='blue-text'>{room}</span>
                                <span className="new badge blue">4</span>
                            </a>);
                    })}
                </div>
            </div>
        );
    }
}

class Messages extends React.Component {
    constructor() {
        super();
        this.state = {
            messages: [["Adolf", "Ahoj :)"], ["Doktorka", "Chci novej kávovar."], ["Vrátná", "Já emaily nečtu."],],
        };
        // Afforix will do this
    }

    render() {
        return (
            <div className="col l6 m4 s12">
                <h3>Messages</h3>
                <div id="messages">
                    {this.state.messages.map((message, index) => {
                        return (
                            message[0] === 'Doktorka' ? ( // condition for printing messages ;)
                                <div className="mine messages" key={index}>
                                    <div className='message'>
                                        <div className="content">{message[1]}</div>
                                    </div>
                                </div>
                            ) : (
                                <div className="yours messages"  key={index}>
                                    <div className='message'>
                                        <div className="sender">{message[0]}</div>
                                        <div className="content">{message[1]}</div>
                                    </div>
                                </div>
                            )
                        );
                    })}
                    <div className='row'>
                        <div className="input-field col s10">
                            <textarea name='message' id='message' className="materialize-textarea" ></textarea>
                            <label htmlFor="message">Your message</label>
                        </div>
                        <div className="input-field col s2">
                            <button className="btn-floating waves-effect waves-light blue">
                                <i className="material-icons">send</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class ActualRoomInfo extends React.Component {
    constructor() {
        super();
        this.state = {
            users: ["Adolf", "Doktorka", "Vrátná"],
        };
        this.activeRoomName = "School";
        // Afforix will do this
    }

    render() {
        return (
            <div className="col l3 m3 s6">
                <h4>{this.activeRoomName}</h4>
                <div className="collection">
                    {this.state.users.map((user, index) => {
                        return (
                            <div className="collection-item" key={user}>
                                {user}
                            </div>);
                    })}
                </div>
                <button className="btn waves-effect waves-light blue" type="submit" name="action">
                    Leave room
                </button>
            </div>
        );
    }
}
export default App;
